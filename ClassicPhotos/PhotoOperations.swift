//
//  PhotoOperations.swift
//  ClassicPhotos
//
//  Created by Ye Ma on 2016-01-27.
//  Copyright © 2016 raywenderlich. All rights reserved.
//

import Foundation
import UIKit
import CoreImage

enum PhotoRecordState {
    case New, Downloaded, Filtered, Failed
}

class PhotoRecord {
    let name: String
    let url: NSURL
    var state = PhotoRecordState.New
    var image = UIImage(named: "Placeholder")
    
    init(name : String, url: NSURL) {
        self.name = name
        self.url = url
    }
}

class PendingOperations {
    lazy var downloadsInProgress = [NSIndexPath : NSOperation]()
    lazy var downloadQueue : NSOperationQueue = {
        var queue = NSOperationQueue()
        queue.name = "Download Queue"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
    lazy var filterationInProgress = [NSIndexPath : NSOperation]()
    lazy var filterationQueue : NSOperationQueue = {
        var queue = NSOperationQueue()
        queue.name = "Image Filteration Queue"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
}

class ImageDownloader: NSOperation {
    
    let photoRecord: PhotoRecord
    
    init(photoRecord: PhotoRecord) {
        self.photoRecord = photoRecord
    }
    
    override func main() {
        if self.cancelled {
            return
        }
        
        let imageData = NSData(contentsOfURL: self.photoRecord.url)
        
        if self.cancelled {
            return
        }
        
        if imageData?.length > 0 {
            self.photoRecord.image = UIImage(data: imageData!)
            self.photoRecord.state = .Downloaded
        }
        else {
            self.photoRecord.image = UIImage(named: "Failed")
            self.photoRecord.state = .Failed
        
        }
    }
}

class ImageFilteration: NSOperation {
    let photoRecord: PhotoRecord
    init(photoRecord:PhotoRecord) {
        self.photoRecord = photoRecord
    }
    
    override func main() {
        if self.cancelled {
            return
        }
        
        if self.photoRecord.state != .Downloaded {
            return
        }
        
        if let filteredImage = self.applySepialFilter(self.photoRecord.image!) {
            self.photoRecord.image = filteredImage
            self.photoRecord.state = .Filtered
        }
    }
    
    func applySepialFilter(image: UIImage) -> UIImage? {
        if let data = UIImagePNGRepresentation(image) {
            let inputImage = CIImage(data: data)
            
            if self.cancelled {
                return nil
            }
            
            let context = CIContext(options:nil)
            
            if let filter = CIFilter(name:"CISepiaTone") {
                filter.setValue(inputImage, forKey: kCIInputImageKey)
                filter.setValue(0.8, forKey: "inputIntensity")
                if let outputImage = filter.outputImage {
                    if self.cancelled {
                        return nil
                    }
                    
                    let outImage = context.createCGImage(outputImage, fromRect: outputImage.extent)
                    
                    let returnImage = UIImage(CGImage: outImage)
                    return returnImage
                }

            }
        }
        return nil
    }
}