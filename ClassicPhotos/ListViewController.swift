//
//  ListViewController.swift
//  ClassicPhotos
//
//  Created by Richard Turton on 03/07/2014.
//  Copyright (c) 2014 raywenderlich. All rights reserved.
//

import UIKit
import CoreImage

let dataSourceURL = NSURL(string:"http://www.raywenderlich.com/downloads/ClassicPhotosDictionary.plist")

class ListViewController: UITableViewController {
  
//  lazy var photos = NSDictionary(contentsOfURL:dataSourceURL!)!
  
    var photos = [PhotoRecord]()
    
    let pendingOperations = PendingOperations()
    
    func fetchPhotoDetails() {
        let request = NSURLRequest(URL: dataSourceURL!)
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) { (response, data, error) -> Void in
            if data != nil {
                do {
                    let datasourceDictionary = try NSPropertyListSerialization.propertyListWithData(data!, options: NSPropertyListMutabilityOptions.Immutable, format: nil) as! NSDictionary
                    
                    for key in datasourceDictionary.allKeys {
                        let name : String = key as! String
                        let value = datasourceDictionary[name] as? String ?? ""
                        let url = NSURL(string: value) ?? NSURL(string : "")
                        let photoRecord = PhotoRecord(name: name, url: url!)
                        self.photos.append(photoRecord)
                    }
                    
                    self.tableView.reloadData()
                }
                catch {
                    print("Failed to load photos list")
                }
            }
            
            if error != nil {
                let alert = UIAlertController(title: "Oops!", message: error?.localizedDescription, preferredStyle: .Alert)
                self.presentViewController(alert, animated: true, completion: nil)
            }
            
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        }
    }
    
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.title = "Classic Photos"
    
    self.fetchPhotoDetails()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
    
    // MARK: - UIScrollViewDelegate method
    
    override func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        suspendAllOperations()
    }
    
    override func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if !decelerate {
            loadImagesForOnscreenCells()
            resumeAllOperations()
        }
    }
    
    override func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        loadImagesForOnscreenCells()
        resumeAllOperations()
    }
  
  // MARK: - Table view data source
  
  override func tableView(tableView: UITableView?, numberOfRowsInSection section: Int) -> Int {
    return photos.count
  }
  
  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("CellIdentifier", forIndexPath: indexPath)
    
    //1
    if cell.accessoryView == nil {
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .Gray)
        cell.accessoryView = indicator
    }
    
    let indicator = cell.accessoryView as! UIActivityIndicatorView
    
    //2
    let photoDetails = self.photos[indexPath.row]
    
    cell.textLabel?.text = photoDetails.name
    cell.imageView?.image = photoDetails.image
    
    switch photoDetails.state {
    case .Filtered:
        indicator.stopAnimating()
    case .Failed:
        indicator.stopAnimating()
    case .New, .Downloaded:
        indicator.startAnimating()
        if (!tableView.dragging && !tableView.decelerating) {
            self.startOperationsForPhotoRecord(photoDetails, indexPath: indexPath)
        }
    }
    
    return cell
  }
    
    // MARK: - helper methods
  
    func startOperationsForPhotoRecord(photoDetails : PhotoRecord, indexPath : NSIndexPath) {
        switch photoDetails.state {
        case .New:
            startDownloadForRecord(photoDetails, indexPath: indexPath)
        case .Downloaded:
            startFilterationForRecord(photoDetails, indexPath: indexPath)
        default:
            print("does nothing")
        }
    }
    
    func startDownloadForRecord(photoDetails : PhotoRecord, indexPath : NSIndexPath) {
        
        //if already in queue, return
        if let _ = pendingOperations.downloadsInProgress[indexPath] {
            return
        }
        
        let downloader = ImageDownloader(photoRecord: photoDetails)
        
        downloader.completionBlock = {
            if downloader.cancelled {
                return
            }
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.pendingOperations.downloadsInProgress.removeValueForKey(indexPath)
                self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            })
            
        }
        
        self.pendingOperations.downloadsInProgress[indexPath] = downloader
        self.pendingOperations.downloadQueue.addOperation(downloader)
    }
    
    func startFilterationForRecord(photoDetails : PhotoRecord, indexPath : NSIndexPath) {
        
        if let _ = pendingOperations.filterationInProgress[indexPath] {
            return;
        }
        
        let filterer = ImageFilteration(photoRecord: photoDetails)
        
        filterer.completionBlock = {
            if filterer.cancelled {
                return
            }
            
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                self.pendingOperations.filterationInProgress.removeValueForKey(indexPath)
                self.tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
            })
            
        }
        
        self.pendingOperations.filterationInProgress[indexPath] = filterer
        self.pendingOperations.filterationQueue.addOperation(filterer)
        
    }
    
  func applySepiaFilter(image:UIImage) -> UIImage? {
    if let data = UIImagePNGRepresentation(image) {
        let inputImage = CIImage(data:data)
        let context = CIContext(options:nil)
        if let filter = CIFilter(name:"CISepiaTone") {
            filter.setValue(inputImage, forKey: kCIInputImageKey)
            filter.setValue(0.8, forKey: "inputIntensity")
            if let outputImage = filter.outputImage {
                let outImage = context.createCGImage(outputImage, fromRect: outputImage.extent)
                return UIImage(CGImage: outImage)
            }
        }
    }
    return nil
    
  }
  
    func suspendAllOperations() {
    
        pendingOperations.downloadQueue.suspended = true
        pendingOperations.filterationQueue.suspended = true
    
    }

    func resumeAllOperations() {
        
        pendingOperations.downloadQueue.suspended = false
        pendingOperations.filterationQueue.suspended = false
        
    }
    
    func loadImagesForOnscreenCells() {
        
        if let pathsArray = tableView.indexPathsForVisibleRows {
            
            //2
            
            var allPendingOperations = Set(pendingOperations.downloadsInProgress.keys)
            allPendingOperations.unionInPlace(pendingOperations.filterationInProgress.keys)
            
            //3
            var toBeCanceled = allPendingOperations
            let visiblePaths = Set(pathsArray)
            toBeCanceled.subtractInPlace(visiblePaths)
            
            var toBeStarted = visiblePaths
            toBeStarted.subtractInPlace(allPendingOperations)
            
            for indexPath in toBeCanceled {
                if let pendingDownload = pendingOperations.downloadsInProgress[indexPath] {
                    pendingDownload.cancel()
                }
                
                pendingOperations.downloadsInProgress.removeValueForKey(indexPath)
                
                if let pendingFilteration = pendingOperations.filterationInProgress[indexPath] {
                    pendingFilteration.cancel()
                }
                
                pendingOperations.filterationInProgress.removeValueForKey(indexPath)
            
            }
            
            for indexPath in toBeStarted {
                let indexPath = indexPath as NSIndexPath
                let recordToProcess = self.photos[indexPath.row]
                startOperationsForPhotoRecord(recordToProcess, indexPath: indexPath)
                
            }
        }
    }
}
