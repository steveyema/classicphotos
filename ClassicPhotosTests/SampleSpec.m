//
//  SampleSpec.m
//  ClassicPhotos
//
//  Created by Ye Ma on 2016-02-26.
//  Copyright © 2016 raywenderlich. All rights reserved.
//

#import "Kiwi.h"

SPEC_BEGIN(MathSec)

describe(@"Math", ^{
    it(@"is pretty cool", ^{
        NSUInteger a = 16;
        NSUInteger b = 26;
        [[theValue(a + b) should] equal:theValue(42)];
    });
});

SPEC_END